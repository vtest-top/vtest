import django_filters
from django_filters.rest_framework import FilterSet

from mockapi.models import MockInit, MockConfig


from django import forms

class MockInitFilter(FilterSet):
    kw = django_filters.CharFilter(field_name='name', lookup_expr='icontains')

    class Meta:
        model = MockInit
        fields = ['kw', ]


class MockFilter(FilterSet):
    kw = django_filters.CharFilter(field_name='name', lookup_expr='icontains')

    class Meta:
        model = MockConfig
        fields = ['kw', ]
