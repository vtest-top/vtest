from mockapi.models import MockConfig
import json
import math
from django.http import JsonResponse
from django_filters.rest_framework import DjangoFilterBackend
from vtest.settings import REST_FRAMEWORK

from interfaces.mixins.vtest_model_mixin import BaseModelViewSet, request_param
from interfaces.paginations import CommonPagination
from interfaces.auth import IsLoginAuthenticated
from interfaces.utils.api_response import FormatResponse
from interfaces.utils.myip import get_ip

from mockapi.models import MockInit, MockConfig
from mockapi.serializers import MockSerializer, MockInitSerializer
from mockapi.filter import MockInitFilter, MockFilter

# Create your views here.

FRM_RES = FormatResponse()


def mock_api(request):
    # api_url = request.path
    api_url = request.path[8:]
    api_method = request.method
    req_method_dict = {
        'GET': 1,
        'POST': 2,
        'PUT': 3,
        'DELETE': 4,
    }
    ret_error = {"error": 201, "msg": None}
    if api_method in req_method_dict.keys():
        api_int = req_method_dict[api_method]
    else:
        ret_error['msg'] = '只支持请求方式：【GET, POST, PUT, DELETE】'
        return JsonResponse(ret_error)

    mockapi_obj = MockConfig.objects.filter(url=api_url).filter(
        method=api_int).first()
    if mockapi_obj:
        try:
            res = json.loads(mockapi_obj.response)
            if mockapi_obj.is_init:
                mockinit_obj = MockInit.objects.filter(
                    pk=mockapi_obj.init_id).first()
                mockapi_obj.response = mockinit_obj.init
            mockapi_obj.count += 1
            mockapi_obj.save()
            return JsonResponse(res)
        except Exception as err:
            ret_error['msg'] = "只支持返回json格式"
            ret_error['errorMsg'] = str(err)
            return JsonResponse(ret_error)
    else:
        return JsonResponse({"error": 201, "msg": "请求方式与URL不匹配..."})


class MockInitViewSet(BaseModelViewSet):
    permission_classes = (IsLoginAuthenticated, )
    queryset = MockInit.objects.all()
    serializer_class = MockInitSerializer
    pagination_class = CommonPagination
    filter_backends = (DjangoFilterBackend, )
    filter_class = MockInitFilter

    def destroy(self, request, *args, **kwargs):
        try:
            instance = self.get_object()
            pid = instance.id

            project_obj = MockConfig.objects.filter(init_id=int(pid)).first()
            if project_obj:
                return FRM_RES.not_allow_delete(msg='删除失败：存在关联关系，请取消关联后再删除！')
            self.perform_destroy(instance)
            return FRM_RES.ok_response()
        except Exception as err:
            try:
                kw = request.query_params['kw']
                return FRM_RES.null_response()
            except Exception as err:
                return FRM_RES.base_response()

    def perform_destroy(self, instance):
        instance.delete()


class MockViewSet(BaseModelViewSet):
    permission_classes = (IsLoginAuthenticated, )
    queryset = MockConfig.objects.all()
    serializer_class = MockSerializer
    pagination_class = CommonPagination
    filter_backends = (DjangoFilterBackend, )
    filter_class = MockFilter

    def list(self, request, *args, **kwargs):

        for req_key in request.query_params.keys():
            if req_key not in request_param:
                return FRM_RES.bad_request(data={'message': '请求参与有误！'})

        try:
            page_num = request.query_params['page']
        except Exception as err:
            page_num = '1'

        if page_num.isdigit():
            page_num = int(page_num)
        else:
            return FRM_RES.invalid_url_param()

        page_size = REST_FRAMEWORK['PAGE_SIZE']
        queryset = self.filter_queryset(self.get_queryset())
        # page_count = len(queryset)
        # max_page = math.ceil(page_count / page_size)
        # print(page_num, max_page)
        # if page_num < 0 or page_num > max_page:
        #     return FRM_RES.error_page_number()
        try:
            # base_url = get_ip()
            page = self.paginate_queryset(queryset)
            if page is not None:

                serializer = self.get_serializer(page, many=True)
                page_response = self.get_paginated_response(serializer.data)
                page_count = math.ceil(page_response.data['count'] / page_size)
                for item in page_response.data['results']:
                    item['url'] = '/mockapi' + item['url']
                return FRM_RES.ok_list_response(page_response.data)

            serializer = self.get_serializer(queryset, many=True)
            return FRM_RES.ok_response(serializer.data)
        except Exception as err:
            try:
                kw = request.query_params['kw']
                if kw:
                    return FRM_RES.null_response()
            except Exception as e:
                return FRM_RES.bad_request(data=err)
