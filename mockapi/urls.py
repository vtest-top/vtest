
from django.urls import re_path, path, include
from mockapi import views
from rest_framework import routers

app_name = 'mockapi'

router = routers.DefaultRouter(trailing_slash=False)
router.register('init', views.MockInitViewSet)
router.register('config', views.MockViewSet)

urlpatterns = [
]

urlpatterns += router.urls
