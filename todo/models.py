from django.db import models
from interfaces.models import BaseModel

# Create your models here.

class ProjectInfo(BaseModel):
    project_id = models.IntegerField('项目ID', blank=True)
    type = models.IntegerField('项目类型', blank=True, default=0)  # 0-性能和接口  1-接口 2-性能
    swagger = models.CharField('swagger地址', max_length=128, null=True, blank=True)
    git = models.CharField('git地址', max_length=128, null=True, blank=True)
    status = models.IntegerField('项目状态', blank=True, default=0)  # 0-正常  1-暂停 2-待领取 3-已领取未完成 4-已完成
    remark = models.CharField('备注', max_length=512, null=True, blank=True)
    progress = models.IntegerField('项目进度', blank=True, default=0)
    total_count = models.IntegerField('总数', blank=True, null=True, default=0)
    wait_api_count = models.IntegerField('待领取数', blank=True, null=True, default=0)
    doing_api_count = models.IntegerField('待完成数', blank=True, null=True, default=0)
    done_api_count = models.IntegerField('已完成数', blank=True, null=True, default=0)
    join_count = models.IntegerField('参与人数', blank=True, null=True, default=0)
    join_user = models.CharField('参与人', max_length=1024, null=True, default=None, blank=True)

    class Meta:
        db_table = 't_todo_project_info'
        verbose_name = '项目任务管理'
        verbose_name_plural = '项目任务管理'


class ProjectApi(BaseModel):
    project_info_id = models.IntegerField('项目任务管理ID', blank=True)
    name = models.CharField('接口名称', max_length=256, null=True)
    tags_name = models.CharField('套件名', max_length=256, null=True, default=None, blank=True)
    request_method = models.CharField('请求方式', null=True, max_length=8, default='get')
    api_url = models.CharField('请求路径', max_length=2014)
    api_status = (
        (0, '未领取'),
        (1, '未完成'),
        (2, '已完成')
    )
    status = models.IntegerField('状态', choices=api_status, default=0)   # 0:未领取 1:已领取未完成 2: 已完成
    user_id = models.IntegerField('用户ID', blank=True)
    api_priority = (
        ('H', 'High'),
        ('M', 'Medium'),
        ('L', 'Low')
    )
    priority = models.CharField('优先级', choices=api_priority, default='M', max_length=1)
    remark = models.CharField('备注', max_length=1024, default=None, null=True, blank=True)

    class Meta:
        db_table = 't_todo_project_api'
        verbose_name = '接口管理'
        verbose_name_plural = '接口管理'

    def __str__(self):
        return self.name
    