from os import name
from rest_framework import serializers
from rest_framework.serializers import UniqueTogetherValidator
from rest_framework.validators import UniqueValidator
from blog.models import BlogCategory, BlogTags, Content


class BlogCategorySerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)
    name = serializers.CharField(required=True, min_length=1, max_length=16)

    class Meta:
        model = BlogCategory
        fields = ['id', 'name']


class BlogTagsSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)
    name = serializers.CharField(required=True, min_length=1, max_length=16)

    class Meta:
        model = BlogTags
        fields = ['id', 'name']


class ContentSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)
    title = serializers.CharField(required=True, min_length=1, max_length=128)
    content = serializers.CharField(required=True, )
    type = serializers.CharField(required=True)
    url = serializers.URLField(required=False)
    tags = serializers.StringRelatedField(many=True)
    category_id = serializers.ReadOnlyField(source='category.id')
    category_name = serializers.ReadOnlyField(source='category.name')

    class Meta:
        model = BlogTags
        fields = [
            'id', 'title', 'content', 'type', 'url', 'tags', 'category_id',
            'category_name'
        ]
