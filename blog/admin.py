from django.contrib import admin
from blog.models import BlogCategory, BlogTags, Content

# Register your models here.


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')
    search_fields = ('name', )


class TagsAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')
    search_fields = ('name', )


class ContentAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'content', 'category', 'tags_name', 'type')
    search_fields = ('name', )

    def tags_name(self, obj):
        tags_list = []
        for i in obj.tags.values_list():
            tags_list.append(i[-1])
        return ' | '.join(tags_list)


admin.site.register(BlogTags, TagsAdmin)
admin.site.register(BlogCategory, CategoryAdmin)
admin.site.register(Content, ContentAdmin)
