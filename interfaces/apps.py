from django.apps import AppConfig


class InterfacesConfig(AppConfig):
    name = 'interfaces'
    verbose_name = 'VTest'
    verbose_name_plural = 'VTest'
