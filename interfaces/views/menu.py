from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from interfaces.models import Menu
from interfaces.serializers import MenuSerializer
from interfaces.auth import IsLoginAuthenticated


class MenuViewSet(ModelViewSet):
    permission_classes = (IsLoginAuthenticated, )
    queryset = Menu.objects.all()
    serializer_class = MenuSerializer

    def list(self, request, *args, **kwargs):
        try:
            queryset = self.filter_queryset(self.get_queryset())
            ret = {'code': 200, 'status': True, 'data': []}
            serializer = self.get_serializer(queryset, many=True)

            parent_menu = []
            sub_menu_dict = {}

            for item in serializer.data:               
                sub_menu_list = []

                if item['level'] == 0:                    
                    parent_menu.append({
                        'id': item['id'],
                        'name': item['name'],
                        'level': item['level'],
                        'parent_id': None,
                        'child_menus': []
                    })

                    if item['id'] not in sub_menu_dict.keys():
                        sub_menu_dict[item['id']] = []                        
                else:
                    if item['parent_id'] not in sub_menu_dict.keys():
                        sub_menu_dict[item['parent_id']] = []
                        
                    sub_menu_dict[item['parent_id']].append(
                        {
                            'id': item['id'],
                            'name': item['name'],
                            'level': item['level'],
                            'parent_id': item['parent_id'],
                            'path': item['path']
                        }
                    )

            for sub_menu in parent_menu:
                sub_menu['child_menus'] = sub_menu_dict[sub_menu['id']]
                
            return Response({
                'code': 200,
                'status': True,
                'message': '获取数据成功。',
                'data': parent_menu
            })
        except Exception as err:
            return Response({
                'code': 500,
                'status': False,
                'message': '获取数据异常，请联系管理员。',
                'data': None
            })
