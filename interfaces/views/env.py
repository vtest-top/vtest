from django_filters.rest_framework import DjangoFilterBackend

from interfaces.models import Env
from interfaces.serializers import EnvSerializer
from interfaces.mixins.vtest_model_mixin import BaseModelViewSet
from interfaces.paginations import CommonPagination
from interfaces.auth import IsLoginAuthenticated
from interfaces.filters import EnvFilter


class EnvViewSet(BaseModelViewSet):
    permission_classes = (IsLoginAuthenticated, )
    queryset = Env.objects.all()
    serializer_class = EnvSerializer
    pagination_class = CommonPagination
    filter_backends = (DjangoFilterBackend, )
    filter_class = EnvFilter
