from rest_framework.response import Response
from rest_framework.authtoken.views import AuthTokenSerializer
from rest_framework.authtoken.models import Token
from rest_framework.views import APIView


class LoginView(APIView):
    throttle_classes = ()
    permission_classes = ()
    serializer_class = AuthTokenSerializer

    def post(self, request, *args, **kwargs):
        try:
            serializer = self.serializer_class(data=request.data,
                                               context={'request': request})
            serializer.is_valid(raise_exception=True)
            user = serializer.validated_data['user']
            token, created = Token.objects.get_or_create(user=user)
            if user.first_name:
                username = user.first_name
                if len(user.first_name) <= 2:
                    shortname = user.first_name
                else:
                    shortname = user.first_name[-2:]
            else:
                username = user.username
                shortname = '大佬'
            return Response({
                'code': 200,
                'status': True,
                'message': '登录成功',
                'token': token.key,
                'data': {
                    'username': user.username,
                    'shortname': shortname,
                    'email': user.email,
                    'name': username
                }
            })
        except Exception as err:
            return Response({
                'code': 201,
                'status': False,
                'message': '用户名或密码有误',
                'token': None,
                'data': None
            })
