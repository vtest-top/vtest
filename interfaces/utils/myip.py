import socket

def get_ip():
    """获取当前服务器的IP地址"""
    hostname = socket.getfqdn(socket.gethostname())
    ip = socket.gethostbyname(hostname)
    return ip
