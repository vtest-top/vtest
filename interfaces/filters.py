import django_filters
from django_filters.rest_framework import FilterSet

from interfaces.models import Department, Env, Project, ProjectSystem, Version


from django import forms

class DepartmentFilter(FilterSet):
    kw = django_filters.CharFilter(field_name='name', lookup_expr='icontains')

    class Meta:
        model = Department
        fields = ['kw', ]


class EnvFilter(FilterSet):
    kw = django_filters.CharFilter(field_name='name', lookup_expr='icontains')

    class Meta:
        model = Env
        fields = ['kw', ]


class ProjectFilter(FilterSet):
    kw = django_filters.CharFilter(field_name='name', lookup_expr='icontains')

    class Meta:
        model = Project
        fields = ['kw', ]


class ProjectSystemFilter(FilterSet):
    kw = django_filters.CharFilter(field_name='name', lookup_expr='icontains')

    class Meta:
        model = ProjectSystem
        fields = ['kw', ]


class VersionFilter(FilterSet):
    kw = django_filters.CharFilter(field_name='version', lookup_expr='icontains')

    class Meta:
        model = Version
        fields = ['kw', ]