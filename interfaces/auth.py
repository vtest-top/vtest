from rest_framework.authtoken.models import Token

class IsLoginAuthenticated:
    """
    Allows access only to authenticated users.
    """

    def has_permission(self, request, view):
        ret = False
        try:
            token = request._request.headers['Authorization']
            if token:
                is_valid = Token.objects.filter(key=token).first()
                if is_valid:
                    ret = True
        except Exception as err:
            pass
        return ret
    
    def has_object_permission(self, request, view, obj):
        """
        Return `True` if permission is granted, `False` otherwise.
        """
        return True

class NoAuthenticated:
    """
    Allows access only to authenticated users.
    """

    def has_permission(self, request, view):
        return True
    
    def has_object_permission(self, request, view, obj):
        """
        Return `True` if permission is granted, `False` otherwise.
        """
        return True