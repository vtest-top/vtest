from django.db import models

# Create your models here.


class BaseModel(models.Model):
    create_time = models.DateTimeField('创建时间', auto_now=True)
    update_time = models.DateTimeField('更新时间', auto_now_add=True)

    class Meta:
        abstract = True


class Menu(BaseModel):
    name = models.CharField('菜单名称', max_length=16, blank=True)
    level_choices = ((0, '一级菜单'), (1, '二级菜单'))
    level = models.IntegerField('等级', choices=level_choices, default=0)
    parent_id = models.IntegerField('父ID', null=True, default=None, blank=True)
    path = models.CharField('路径', max_length=255, default='/', blank=True)

    class Meta:
        db_table = 't_menu'
        verbose_name = '菜单管理'
        verbose_name_plural = '菜单管理'

    def __str__(self):
        return self.name


class Department(BaseModel):
    name = models.CharField('部门名称', unique=True, max_length=32, blank=True)

    class Meta:
        db_table = 't_department'
        verbose_name = '部门管理'
        verbose_name_plural = '部门管理'

    def __str__(self):
        return self.name


class Env(BaseModel):
    name = models.CharField('环境名称', unique=True, max_length=32, blank=True)
    address = models.CharField('IP/域名', max_length=64, blank=True)

    class Meta:
        db_table = 't_env'
        verbose_name = '环境管理'
        verbose_name_plural = '环境管理'

    def __str__(self):
        return self.name


class Project(BaseModel):
    name = models.CharField('环境名称', unique=True, max_length=32, blank=True)
    # department = models.ForeignKey(Department, on_delete=models.CASCADE, null=True)
    department_id = models.IntegerField('部门ID', default=0)
    # department_name = models.CharField('部门名称', max_length=128, null=True, default=0)
    remark = models.CharField('备注', max_length=128, blank=True)

    class Meta:
        db_table = 't_project'
        verbose_name = '项目管理'
        verbose_name_plural = '项目管理'

    def __str__(self):
        return self.name


class ProjectSystem(BaseModel):
    name = models.CharField('系统名称', unique=True, max_length=32, blank=True)
    project_id = models.IntegerField('项目ID', default=0)

    class Meta:
        db_table = 't_project_system'
        verbose_name = '项目模块管理'
        verbose_name_plural = '项目模块管理'

    def __str__(self):
        return self.name


class Version(BaseModel):
    version = models.CharField('版本', max_length=128, blank=True)
    project_id = models.IntegerField('项目ID', default=0)
    submit_test_date = models.DateField('提测时间', null=True, blank=True)
    st_done_date = models.DateField('完成测试时间', null=True, blank=True)
    uat_date = models.DateField('UAT时间', null=True, blank=True)
    product_manager = models.CharField('产品经理', max_length=128, null=True, blank=True)
    project_manager = models.CharField('项目经理', max_length=128, null=True, blank=True)
    frontend_dev = models.CharField('前端负责人', max_length=128, null=True, blank=True)
    backend_dev = models.CharField('后端负责人', max_length=128, null=True, blank=True)
    operation_dev = models.CharField('运维负责人', max_length=128, null=True, blank=True)
    tester = models.CharField('测试负责人', max_length=128, null=True, blank=True)
    jql = models.CharField('jira查询语句', max_length=1024, null=True, blank=True)
    test_time_consuming = models.IntegerField('测试耗时', null=True, blank=True)
    test_progress = models.IntegerField('测试进度', null=True, blank=True)

    class Meta:
        db_table = 't_version'
        verbose_name = '版本管理'
        verbose_name_plural = '版本管理'

    def __str__(self):
        return self.version


class TestModule(BaseModel):
    version_id = models.IntegerField('版本ID', null=True, blank=True)
    name = models.CharField('模块名称', max_length=128, blank=True)
    is_level = models.BooleanField('是否为一级菜单')
    is_testing = models.BooleanField('是否提测')
    test_progress = models.IntegerField('测试进度', null=True, blank=True)

    class Meta:
        db_table = 't_test_module'
        verbose_name = '测试模块管理'
        verbose_name_plural = '测试模块管理'

    def __str__(self):
        return self.name
