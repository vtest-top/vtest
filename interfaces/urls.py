from django.urls import path, re_path
from rest_framework import routers

from interfaces.views.user import LoginView
from interfaces.views.menu import MenuViewSet
from interfaces.views.department import DepartmentViewSet
from interfaces.views.env import EnvViewSet
from interfaces.views.project import ProjectViewSet, ProjecSystemtViewSet, VersionViewSet


router = routers.DefaultRouter(trailing_slash=False)
router.register('menus', MenuViewSet)
router.register('department', DepartmentViewSet)
router.register('department', DepartmentViewSet)
router.register('env', EnvViewSet)
router.register('project', ProjectViewSet)
router.register('projectSystem', ProjecSystemtViewSet)
router.register('version', VersionViewSet)

urlpatterns = [
    re_path('user/login$', LoginView.as_view()),
    # re_path('menus', MenuViewSet, name='menu_list')
]
urlpatterns += router.urls
