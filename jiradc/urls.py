from django.urls import path, include
from jiradc import views
from rest_framework import routers

router = routers.DefaultRouter(trailing_slash=False)
router.register('jql', views.JQLViewSet)
router.register('download', views.DownloadXlsViewSet)

urlpatterns = [
    path('bugs', views.analysis_bugs),
]

urlpatterns += router.urls