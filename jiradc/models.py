from django.db import models
from interfaces.models import BaseModel

# Create your models here.


class JiraRecord(BaseModel):
    jql = models.CharField('JQL语句', null=True, max_length=10240)
    result = models.CharField('结果', null=True, default='失败', max_length=16)  # 0成功 1失败
    response = models.CharField('返回数据', null=True, max_length=1024000000)

    class Meta:
        db_table = 't_jira_record'
        verbose_name = 'Jira记录'
        verbose_name_plural = 'Jira记录'

    def __str__(self):
        return self.jql


class JQL(BaseModel):
    name = models.CharField('名称', unique=True, null=True, max_length=32)
    jql = models.CharField('JQL语句', null=True, max_length=10240)

    class Meta:
        db_table = 't_jira_jql'
        verbose_name = 'JQL记录'
        verbose_name_plural = 'JQL记录'

    def __str__(self):
        return self.name
