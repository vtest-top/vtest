import django_filters
from django_filters.rest_framework import FilterSet

from jiradc.models import JQL


from django import forms

class JQLFilter(FilterSet):
    kw = django_filters.CharFilter(field_name='name', lookup_expr='icontains')

    class Meta:
        model = JQL
        fields = ['kw', ]

