from rest_framework import serializers
from rest_framework.serializers import UniqueTogetherValidator
from rest_framework.validators import UniqueValidator
from jiradc.models import JQL, JiraRecord


class JQLSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)
    name = serializers.CharField(required=True, min_length=1, max_length=32)
    jql = serializers.CharField(required=True, min_length=3, max_length=1024)

    class Meta:
        model = JQL
        fields = ['id', 'name', 'jql']

class JiraRecordSerializer(serializers.ModelSerializer):

    class Meta:
        model = JiraRecord
        fields = ['id', 'jql', 'result', 'response']
