import os
import time
import json

from django.shortcuts import HttpResponse
from django_filters.rest_framework import DjangoFilterBackend


from rest_framework.response import Response
from rest_framework.views import APIView

from interfaces.utils.api_response import FormatResponse
from interfaces.mixins.vtest_model_mixin import BaseModelViewSet, BaseRetrieveModelMixin
from interfaces.auth import IsLoginAuthenticated, NoAuthenticated
from interfaces.paginations import CommonPagination
from vtest.settings import BASE_DIR

from jiradc.serializers import JQLSerializer, JiraRecordSerializer
from jiradc.filters import JQLFilter
from jiradc.gen_xls import xls
from jiradc.models import JiraRecord, JQL, JiraRecord
from jiradc.jira_handle import jira_report

FRM_RES = FormatResponse()


def get_timestamp(str_len=13):
    """ get timestamp string, length can only between 0 and 16
    """
    if isinstance(str_len, int) and 0 < str_len < 17:
        return str(time.time()).replace(".", "")[:str_len]


class JQLViewSet(BaseModelViewSet):
    permission_classes = (IsLoginAuthenticated, )
    queryset = JQL.objects.all()
    serializer_class = JQLSerializer
    pagination_class = CommonPagination
    filter_backends = (DjangoFilterBackend, )
    filter_class = JQLFilter


def analysis_bugs(request):
    jql = None
    result = '失败'
    if request.method == 'GET':
        jql = request.GET.get('jql')
        if jql:
            if 'order by' not in jql.lower():
                jql = jql + ' ORDER BY priority DESC'
            elif 'priority desc' not in jql.lower():
                jql = jql + ',priority DESC'
            reports = jira_report(jql=jql)
        else:
            reports = {
                'code': 202,
                'message': 'jql为必填项！',
                'bugs_line': {},
                'priority_list': [],
                'open_priority_list': [],
                'no_fixed_list': []
            }
    else:
        reports = {
            'code': 201,
            'message': '请求方式错误，只支持GET请求！',
            'bugs_line': {},
            'priority_list': [],
            'open_priority_list': [],
            'no_fixed_list': []
        }
    if reports['code'] == 200:
        result = '成功'
    reports_json = json.dumps(reports, ensure_ascii=False)
    jrac_record = JiraRecord.objects.create(
        jql=jql, result=result, response=str(reports_json))
    if reports['code'] == 200:
        record_id = jrac_record.pk
    else:
        record_id = 0
    reports['record_id'] = record_id
    reports_json = json.dumps(reports, ensure_ascii=False)

    return HttpResponse(reports_json, content_type='application/json,charset=utf-8')


class DownloadXlsViewSet(BaseModelViewSet):
    permission_classes = (NoAuthenticated, )
    queryset = JiraRecord.objects.all()
    pagination_class = CommonPagination
    serializer_class = JiraRecordSerializer

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        result = serializer.data

        if result['result'] == '成功':
            try:
                str_time = 'bugslist_' + get_timestamp(13) + '.xls'
                filename = BASE_DIR + os.sep + str_time
                buf = xls(result['response'], filename)
            except Exception as err:
                return FRM_RES.not_allow_delete(msg=str(err))
        else:
            return FRM_RES.not_allow_delete(msg='下载失败，JQL查到的数据异常或JQL语句有误！')

        response = HttpResponse(content_type='application/octet-stream')
        response['Content-Disposition'] = 'attachment;filename={}'.format(
            str_time)
        buf.seek(0)
        response.write(buf.getvalue())
        return response
