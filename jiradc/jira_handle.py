# Author: fuqiang523
# Date: 2020-5-14 15:35
# File: test
# Desc:

from jira import JIRA
import pandas
import json
import time


class JiraBugs:
    def __init__(self, jql, max_results=1000):
        self.jira = JIRA(server='http://jiradc.vtest.top/',
                         basic_auth=('vtest', 'vtest@1234'))
        self.jql = jql
        self.max_results = max_results
        self.resolved_status_list = ['测试通过', '非缺陷', '暂不处理', '该问题不需解决', '无法重现']
        self.no_solve_status_list = ['待修复', '修复中', '待测试', '测试中']
        self.priority_type = ['Highest', 'High', 'Medium', 'Low', 'Lowest']

    def get_issues(self):
        ret = {
            'bug_line': {
                'dateline': [],
                'created': [],
                'fixed': [],
                'todo': []
            },
            'priority_list': {
                'all': {
                    'Highest': 0,
                    'High': 0,
                    'Medium': 0,
                    'Low': 0,
                    'Lowest': 0
                },
                'no_fixed': {
                    'Highest': 0,
                    'High': 0,
                    'Medium': 0,
                    'Low': 0,
                    'Lowest': 0
                }
            },
            'bugs_list': [],
            'today_bug': {
                'created': 0,
                'total': 0,
                'todo': 0,
                'high': 0
            }
        }
        issues = self.jira.search_issues(self.jql, maxResults=self.max_results)
        self.jira.close()
        # 遍历属性
        default_dict = {}
        date_line = []
        reporter_list = []
        developer_list = []
        for issue in issues:
            default_dict[issue.key] = {
                'bug_id': issue.key,  # bug id
                'system': issue.fields.customfield_12806,  # 所属系统环境
                'developer': issue.fields.customfield_10218,  # 缺陷责任人
                'updated': str(issue.fields.updated).split('T')[0],  # 最近更新时间
                # 解决结果
                'resolution': issue.fields.resolution if 'resolution' in dir(issue.fields) else None,
                'status': issue.fields.status.name,  # 状态
                # 解决时间
                'resolution_date': str(issue.fields.resolutiondate).split('T')[0],
                'env': issue.fields.customfield_10210,  # 系统环境说明
                'is_smoking': issue.fields.customfield_10213,  # 是否冒烟测试缺陷
                'components': issue.fields.components,  # 组件
                'project': issue.fields.project,  # 项目
                'summary': issue.fields.summary,  # 标题
                'reporter': issue.fields.reporter.displayName,  # 报告人
                'created': str(issue.fields.created).split('T')[0],  # 创建时间
                'bug_type': issue.fields.customfield_10233,  # 缺陷类型
                # 优先级
                'priority': issue.fields.priority.name if 'priority' in dir(issue.fields) else None,
                'creator': issue.fields.creator,  # 创建人
                # 经办人
                'assignee': issue.fields.assignee.displayName if 'displayName' in dir(issue.fields.assignee) else None,
                'issue_type': issue.fields.issuetype  # 类型
            }
            if issue.fields.updated:
                date_line.append(str(issue.fields.updated).split('T')[0])
            if issue.fields.resolutiondate:
                date_line.append(
                    str(issue.fields.resolutiondate).split('T')[0])
            if issue.fields.created:
                date_line.append(str(issue.fields.created).split('T')[0])
            reporter_list.append(issue.fields.reporter.displayName)
            for i in issue.fields.customfield_10218:
                developer_list.append(i.displayName)
        date_line = list(set(date_line))
        date_line.sort()
        reporter_list = list(set(reporter_list))
        developer_list = list(set(developer_list))
        reporter_dict = {}
        developer_dict = {}
        for reporter in reporter_list:
            reporter_dict[reporter] = 0
        for developer in developer_list:
            developer_dict[developer] = {
                'total': 0,
                'fixed': 0,
                'no_fix': 0
            }
        bug_line = {'created': {}, 'fixed': {}, 'todo': {}}
        for i_date in date_line:
            bug_line['created'][i_date] = 0
            bug_line['fixed'][i_date] = 0
            bug_line['todo'][i_date] = 0

        high_bug = 0
        todoy_created = 0
        today_date = time.strftime("%Y-%m-%d", time.localtime())
        for bugid in default_dict.keys():
            bug_dict = default_dict[bugid]
            reporter_dict[bug_dict['reporter']] += 1
            for i_dev in bug_dict['developer']:
                developer_dict[i_dev.displayName]['total'] += 1
                if bug_dict['status'] in self.resolved_status_list:
                    developer_dict[i_dev.displayName]['fixed'] += 1
                else:
                    developer_dict[i_dev.displayName]['no_fix'] += 1
            if bug_dict['priority'] in self.priority_type:
                ret['priority_list']['all'][bug_dict['priority']] += 1
                if bug_dict['status'] in self.no_solve_status_list:
                    if bug_dict['priority'] in ['High', 'Highest']:
                        high_bug += 1
                    ret['priority_list']['no_fixed'][bug_dict['priority']] += 1
                    bug_list = {
                        'bugid': bugid,
                        'summary': bug_dict['summary'],
                        'assignee': bug_dict['assignee'],
                        'reporter': bug_dict['reporter'],
                        'priority': bug_dict['priority'],
                        'status': bug_dict['status'],
                        'created': bug_dict['created']
                    }
                    ret['bugs_list'].append(bug_list)
                bug_line['created'][bug_dict['created']] += 1
                if bug_dict['status'] in self.resolved_status_list:
                    if bug_dict['resolution_date'] != 'None':
                        bug_line['fixed'][bug_dict['resolution_date']] += 1
                    else:
                        bug_line['fixed'][bug_dict['updated']] += 1
                if bug_dict['created'] == today_date:
                    todoy_created += 1
        created_count_list = []
        fixed_count_list = []
        todo_count_list = []
        for i in date_line:
            if i in bug_line['created'].keys():
                created_count_list.append(bug_line['created'][i])
            else:
                created_count_list.append(0)
            if i in bug_line['fixed'].keys():
                fixed_count_list.append(bug_line['fixed'][i])
            else:
                fixed_count_list.append(0)
        difference_list = [
            created_count_list[i] - fixed_count_list[i]
            for i in range(len(created_count_list))
        ]
        # difference_list = []
        # for i in len(created_count_list):
        #     difference_list.append(created_count_list[i] - fixed_count_list[i])
        todo_count_list = [
            sum(difference_list[:i + 1]) for i in range(len(difference_list))
        ]
        new_data_line = []
        for item in date_line:
            new_data_line.append(item[5:])
        total_bug = 0
        for i in created_count_list:
            total_bug += i
        dev_list = []
        dev_dict = {}
        for i_key in developer_dict.keys():
            dev_dict = {
                'name': i_key,
                'total': developer_dict[i_key]['total'],
                'fixed': developer_dict[i_key]['fixed'],
                'no_fix': developer_dict[i_key]['no_fix'],
            }
            dev_list.append(dev_dict)
        tester_list = []
        tester_dict = {}
        for j_key in reporter_dict.keys():
            tester_dict = {
                'name': j_key,
                'count': reporter_dict[j_key]
            }
            tester_list.append(tester_dict)
        ret['today_bug']['total'] = total_bug
        ret['today_bug']['high'] = high_bug
        ret['today_bug']['created'] = todoy_created
        ret['today_bug']['todo'] = int(todo_count_list[-1])
        ret['bug_line']['dateline'] = new_data_line
        ret['bug_line']['created'] = created_count_list
        ret['bug_line']['fixed'] = fixed_count_list
        ret['bug_line']['todo'] = todo_count_list
        ret['tester'] = tester_list
        ret['developer'] = dev_list
        return ret


def jira_report(jql, max_results=1000):
    jira_obj = JiraBugs(jql, max_results=max_results)

    try:
        ret = jira_obj.get_issues()
        ret['code'] = 200
        ret['message'] = '请求成功！'
    except Exception as err:
        ret = {
            'code': 201,
            'message': '获取数据失败：请检查网络或JQL语句是否正确！',
            'errMsg': str(err),
            'bug_line': {},
            'priority_list': [],
            'open_priority_list': [],
            'no_fixed_list': []
        }
    return ret


# if __name__ == '__main__':
#     jql = 'text ~ INTGOV-10109 ORDER BY created DESC'
#     x = jira_report(jql)
#     print(x)
#     jira_demo = JiraBugs(jql)
#     x = jira_demo.get_issues()
#     import json
#     x = json.dumps(x)
#     print(x)
#     print(y)
